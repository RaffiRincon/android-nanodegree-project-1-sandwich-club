package com.udacity.sandwichclub;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.ViewManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.udacity.sandwichclub.model.Sandwich;
import com.udacity.sandwichclub.utils.JsonUtils;

import java.util.List;

public class DetailActivity extends AppCompatActivity {

    public static final String EXTRA_POSITION = "extra_position";
    private static final int DEFAULT_POSITION = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ImageView ingredientsIv = findViewById(R.id.image_iv);

        Intent intent = getIntent();
        if (intent == null) {
            closeOnError();
        }

        int position = intent.getIntExtra(EXTRA_POSITION, DEFAULT_POSITION);
        if (position == DEFAULT_POSITION) {
            // EXTRA_POSITION not found in intent
            closeOnError();
            return;
        }

        String[] sandwiches = getResources().getStringArray(R.array.sandwich_details);
        String json = sandwiches[position];
        Sandwich sandwich = JsonUtils.parseSandwichJson(json);
        if (sandwich == null) {
            // Sandwich data unavailable
            closeOnError();
            return;
        }

        populateUI(sandwich);
        Picasso.with(this)
                .load(sandwich.getImage())
                .error(R.drawable.icon_image_loading_error24dp)
                .into(ingredientsIv);

        setTitle(sandwich.getMainName());
    }

    private void closeOnError() {
        finish();
        Toast.makeText(this, R.string.detail_error_message, Toast.LENGTH_SHORT).show();
    }

    private void populateUI(Sandwich sandwich) {
        String otherNames = formattedStringFromList(sandwich.getAlsoKnownAs());
        setText(R.id.also_known_tv, otherNames);
        
        String description = replaceEscapedQuotationMarks(sandwich.getDescription());
        setText(R.id.description_tv, description);
        
        String ingredients = formattedStringFromList(sandwich.getIngredients());
        setText(R.id.ingredients_tv, ingredients);
        
        String origin = replaceEscapedQuotationMarks(sandwich.getPlaceOfOrigin());
        setText(R.id.origin_tv, origin);
    }
    
    private void setText(int tvId, CharSequence text) {
        TextView alsoKnownAsTV = findViewById(tvId);
        
        alsoKnownAsTV.setText(text);
        
        if (text == null || text.length() == 0) {
            CardView card = (CardView) alsoKnownAsTV.getParent().getParent();
            
            remove(card);
        }
    }
    
    private void remove(View view) {
        ((ViewManager)view.getParent()).removeView(view);
    }
    
    
    private String formattedStringFromList(List<String> list) {
        if (list == null || list.size() == 0) return null;
        
        StringBuilder result = new StringBuilder();
        result.append(list.get(0));
        for (int i = 1; i < list.size(); i++) {
            result.append("\n").append(list.get(i));
        }
        
        return replaceEscapedQuotationMarks(result.toString());
    }

    private String replaceEscapedQuotationMarks(String original) {
        if (original == null || original.length() == 0) {
            return null;
        }
        
        return original.replace("\\\"", "\"");
    }
    
}
