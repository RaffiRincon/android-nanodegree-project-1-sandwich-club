package com.udacity.sandwichclub.utils;

import android.nfc.Tag;
import android.util.Log;

import com.udacity.sandwichclub.model.Sandwich;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class JsonUtils {
    
    static final String TAG = JsonUtils.class.getName();

    
    // parse Sandwich Object from JSON without using 3rd-party library, as per Udacity assignment requirement
    public static Sandwich parseSandwichJson(String json) {
        Log.d(TAG, "JSON: " + json );
        
        Sandwich sandwich = new Sandwich();
        try {
            JSONObject sandwichJSON = new JSONObject(json);
            JSONObject nameJSON = sandwichJSON.getJSONObject("name");
            
            sandwich.setMainName(nameJSON.getString("mainName"));
            sandwich.setPlaceOfOrigin(sandwichJSON.getString("placeOfOrigin"));
            sandwich.setImage(sandwichJSON.getString("image"));
            sandwich.setDescription(sandwichJSON.getString("description"));
    
            JSONArray otherNames = nameJSON.getJSONArray("alsoKnownAs");
            sandwich.setAlsoKnownAs(stringListFromJSONArray(otherNames));
    
            JSONArray ingredients = sandwichJSON.getJSONArray("ingredients");
            sandwich.setIngredients(stringListFromJSONArray(ingredients));
            
        } catch (JSONException jsonException) {
            jsonException.printStackTrace();
        }
        
        Log.d(TAG, "Sandwich: " + sandwich);
        return sandwich;
    }
    
    private static List<String> stringListFromJSONArray(JSONArray from) throws JSONException {
        if (from == null || from.length() == 0) {
            return null;
        }
        
        ArrayList<String> strings = new ArrayList<String>();
        for (int i = 0; i < from.length(); i++) {
            strings.add(from.getString(i));
        }
        
        return strings;
    }
}
